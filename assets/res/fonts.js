export default {
    regular: 'BasierCircle-Regular', //400
    medium: 'BasierCircle-Medium', //500
    bold: 'BasierCircle-Bold',
    semiBold: 'BasierCircle-SemiBold', //600
}